# Počítání I
Úkolem je vytvořit program, který bude kontrolovat příklady z matematiky.

Při procvičování základních početních operací může velmi pomoci počítač. Předpokládáme, že počítač dostane ke kontrole oskenované vypočtené příklady, které procvičují operace sčítání. Úkolem počítače je zkontrolovat, zda je příklad správně. Bohužel, úloha není tak úplně snadná. Při skenování se může část příkladu špatně rozpoznat. OCR pak na dané místo vyplní znak otazník.

Program tedy na vstupu dostane jeden příklad na sčítání s vyplněným výsledkem. Máme jistotu, že operátory `+` a `=` se naskenovaly správně. Čísla se buď rozpoznala správně, nebo jsou nahrazena znakem otazník. Úkolem programu je takový příklad načíst, vyhodnotit a určit, zda je výsledek správně. Pokud je výsledek správně, může existovat více správných náhrad otazníků za číslice. V takovém případě program zobrazí jednu libovolnou takovou možnou náhradu a dále dopočítá i celkový počet existujících možností náhrad. Pokud je výsledek špatně nebo pokud otazníky nelze nahradit tak, aby vznikl správný výsledek, je výsledkem oznámení o špatně vyřešeném příkladu (viz ukázka).

Vstupem je jeden příklad k analýze. Příklad se skládá z několika sčítanců, které jsou oddělené znakem `+`. Za sčítanci je znak rovnítko a vypočtený výsledek. Sčítance i výsledek jsou desítková čísla, ve kterých se může objevit jeden nebo i více znaků otazník. Za vstupní řádkou je odřádkování, ale jinak řádka neobsahuje žádné bílé znaky.

Výstupem programu je buď oznámení o chybně vypočteném příkladu, nebo je zobrazen zadaný příklad s nahrazenými znaky `?` tak, aby byl výsledek správně. Pokud existuje více možných náhrad, program zobrazí libovolnou jednu z nich. Dále je pak zobrazen počet možných náhrad, kterými se dá dostat ke správně vypočtenému příkladu.

Pokud je vstup neplatný, program to musí detekovat a zobrazit chybové hlášení. Chybové hlášení zobrazujte na standardní výstup (ne na chybový výstup). 

**Za chybu považujte:**
* Na vstupu jsou neplatné sčítance nebo výsledek (musí obsahovat pouze číslice `0` až `9` nebo znak `?`).
* Chybí operátor `=`.
* Mezi dvojicí operátorů `+` není žádný sčítanec.
* Chybí sčítanec před operátorem = nebo výsledek za operátorem `=`.
* Na pravé straně za operátorem `=` jsou operátory `+`.
* Zadání obsahuje bílé znaky.

## Ukázka práce programu:
```
> Priklad:
> 123+456=579
> 123+456=579
> Celkem: 1
```

```
> Priklad:
> 123+456=589
> Nelze dosahnout.
```

```
> Priklad:
> 1?3+45?=579
> 123+456=579
> Celkem: 1
```

```
> Priklad:
> 1??+45?=579
> 120+459=579
> Celkem: 10
```

```
> Priklad:
> 1?+4?=??
> 10+40=50
> Celkem: 100
```

```
> Priklad:
> 1?+2?+?3+?4=?5?
> 10+20+03+24=057
> Celkem: 1000
```

```
> Priklad:
> 1234567891234?6789123456789123456789+28927346582364589234651654???=?????????????????????????????????123
> 123456789123406789123456789123456789+28927346582364589234651654334=123456818050753371488046023775111123
> Celkem: 10
```

```
> Priklad:
> 3+4=???
> 3+4=007
> Celkem: 1
```

```
> Priklad:
> ????1+??2=?
> 00001+002=3
> Celkem: 1
```

```
> Priklad:
> ??+??=??
> 00+00=00
> Celkem: 5050
```

```
> Priklad:
> 2?=?1
> 21=21
> Celkem: 1
```

```
> Priklad:
> 12+34+?5=34+5?
> Nespravny vstup.
```

## Poznámky:
* Ukázkové běhy zachycují očekávané výpisy Vašeho programu (tučné písmo) a vstupy zadané uživatelem (základní písmo). Zvýraznění tučným písmem je použité pouze zde na stránce zadání, aby byl výpis lépe čitelný. Váš program má za úkol pouze zobrazit text bez zvýrazňování (bez HTML markupu).
* Znak odřádkování `\n` je i za poslední řádkou výstupu (i za případným chybovým hlášením).
* Problém lze řešit hrubou silou, kdy se jednotlivé otazníky nahrazují ciframi a testuje se správnost výsledku. Takové řešení s výhodou využije rekurzi. Rekurze ale bude generovat velké množství potenciálních řešení, její implementace proto musí být přiměřeně efektivní.
* Číselné vstupní hodnoty nemají omezený počet cifer, není možné je ukládat do standardních celočíselných datových typů (lze je reprezentovat jako string).

