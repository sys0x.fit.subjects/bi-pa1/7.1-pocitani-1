#include <stdio.h>
#include <stdlib.h>

typedef struct Pole{
   struct Pole *next;
   struct Pole *previous;
   char value;
}POLE;

POLE *numbers[101];
int numberOfOperators=0;


void putFromFront(int order,char whatToPutIn){
    POLE* tmp=(POLE*)malloc(sizeof(POLE));

    if(numbers[order]==NULL){
        tmp->previous=NULL;
        if(order!=100) {            //////////////////////////pozor na to!
            numberOfOperators++;
        }

    }else{
        tmp->previous=numbers[order];
        numbers[order]->next=tmp;
    }
    tmp->next=NULL;
    tmp->value=whatToPutIn;
    numbers[order]=tmp;

}


void badInput(){
    printf("Nespravny vstup.\n");
    exit(1);
}
int getInput(){
    int order=1;
    int check=1;
    int wasEqual=0;
    char tmp;
    int badInput2=1;
    int wasEq=0;
    while(check==1) {
        check = scanf("%c", &tmp);
        if(tmp=='='){
            wasEqual=1;
        }
        if((tmp=='+'&&wasEqual==1)){
            badInput();
        }

        if(badInput2>=2||((tmp<48||tmp>57)&&tmp!='?'&&tmp!='='&&tmp!='+'&&tmp!='\n'))badInput();
        if(tmp=='+'){
            badInput2++;
            order++;
        }else if(tmp=='='){
            wasEq++;
            if (wasEq==2)badInput();
            badInput2++;
            order=0;
        }else if(tmp=='\n'){
            check=-1;
        }else{
            badInput2=0;
            putFromFront(order, tmp);
        }
    }

    return 0;
}
void freeBlock(int block){
    POLE* tmp= numbers[block];

    while(numbers[block]!=NULL){
        tmp=tmp->previous;
        free(numbers[block]);


        numbers[block]=tmp;
    }

}
void printPartOfEquasion(int order){
    POLE* tmp=numbers[order];
    while(tmp->previous!=NULL){
        tmp=tmp->previous;
    }

    while(tmp!=NULL){
        if(tmp->value=='?')printf("0");
        else printf("%c",tmp->value);
        tmp=tmp->next;
    }

}
void printAll(){
    POLE * tmp[101];
    int velikostVysledku=0;
    int i,x;
    int sum=0;

    //malloc tmp
    for(i=0;i<numberOfOperators;i++){
        tmp[i]=numbers[i];
    }
    //move vysledek to the start and get how long it is

        while (tmp[0]->previous != NULL) {
            tmp[0] = tmp[0]->previous;
            velikostVysledku++;
        }

    //print numbers
    for (i=1;i<numberOfOperators;i++){
        printPartOfEquasion(i);
        if(i+1!=numberOfOperators) {
            printf("+");
        }
    }

    printf("=");
    char cislo;
    for(x=-1;x<velikostVysledku;x++) {
        //sum together
        for (i = 1; i < numberOfOperators; i++) {
            if(tmp[i]==NULL)continue;
            sum += tmp[i]->value - '0';
            tmp[i]=tmp[i]->previous;
        }
        cislo=(char)((sum%10)+'0');
        putFromFront(100, cislo);
        sum=sum/10;

    }
    //print vysledek
    tmp[100]=numbers[100];
    while(tmp[100]!=NULL){
        printf("%c",tmp[100]->value);
        tmp[100]=tmp[100]->previous;
    }

    freeBlock(100);

    printf("\n");

}
int vypsano=0;
int rekurze(int whatToSum,int prenos){
    POLE *tmp[50];
    int pocetMozosti=0;
    int i,whereAreWe,x;
    int sum=prenos;
    //malloc tmp
    for(i=0;i<numberOfOperators;i++){
        tmp[i]=numbers[i];
    }
    //move to the right position
    for(i=0;i<numberOfOperators;i++) {
        for (whereAreWe = 0; whereAreWe < whatToSum; whereAreWe++) {
            if (tmp[i] != NULL) {
                tmp[i] = tmp[i]->previous;
            }
        }
    }
    //sum together
    for(i=1;i<numberOfOperators;i++){
        if(tmp[i]==NULL) continue;
        if(tmp[i]->value!='?'){
            sum+=tmp[i]->value-'0';
        }
        else{
            for(x=0;x<=9;x++){
                tmp[i]->value=(char)(x+'0');
                pocetMozosti+=rekurze(whatToSum,prenos);
            }
            tmp[i]->value='?';
            return pocetMozosti;
        }
    }

    //check if correct sum

    if(tmp[0]->value!='?'){
        if((tmp[0]->value-'0')==(sum % 10)){
            prenos=sum/10;

            if(tmp[0]->previous==NULL){
                if(prenos==0){
                    if(vypsano==0) {
                        vypsano = 1;
                        printAll();
                    }
                    return 1;


                }else{
                    return 0;
                }
            }
            whatToSum++;
            pocetMozosti+=rekurze(whatToSum,prenos);
        }else{
            return 0;
        }
    }else{
        prenos=sum/10;

        if(tmp[0]->previous==NULL){
            if(prenos==0){
                if(vypsano==0) {
                    vypsano = 1;
                    printAll();
                }
                return 1;


            }else{
                return 0;
            }
        }
        whatToSum++;
        pocetMozosti+=rekurze(whatToSum,prenos);
    }
    return pocetMozosti;

}
int main() {
    printf("Priklad:\n");
    getInput();
    if(numbers[0]==NULL||numbers[1]==NULL){
        badInput();
    }

    int pocetMoznosti;

    pocetMoznosti=rekurze(0,0);

    if(pocetMoznosti==0){
        printf("Nelze dosahnout.\n");
        return 0;
    }
    printf("Celkem: %d\n",pocetMoznosti);

    return 0;
}